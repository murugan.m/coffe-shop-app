package com.example.coffe_shop_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    var amount:Int = 0
    var coffee:CheckBox? = null
    var tea:CheckBox? = null
    var milk:CheckBox? = null
    var balckTea:CheckBox? = null
    var amountText: TextView? = null
    var coffeeAmountText: TextView? = null
    var teaAmountText: TextView? = null
    var milkAmountText: TextView? = null
    var blackTeaAmountText: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        title = "Free Coffee Shop"
        coffeeAmountText = findViewById(R.id.filterCoffeeValue)
        teaAmountText = findViewById(R.id.teaCheckBoxValue)
        milkAmountText = findViewById(R.id.milkCheckBoxValue)
        blackTeaAmountText = findViewById(R.id.blackTeaCheckBoxValue)

        amountText = findViewById(R.id.amountTextView)
        coffee = findViewById(R.id.filterCoffeeCheckBox)

        coffee?.setOnClickListener {
            if(coffee?.isChecked ?: false) {
                amount += 100
            } else {
                amount -= 100
            }
            checkAndUpdateAmount()
        }

        tea = findViewById(R.id.teaCheckBox)
        tea?.setOnClickListener {
            if(tea?.isChecked ?: false) {
                amount += 50
            } else {
                amount -= 50
            }
            checkAndUpdateAmount()
        }
        milk = findViewById(R.id.milkCheckBox)
        milk?.setOnClickListener {
            if(milk?.isChecked ?: false) {
                amount += 40
            } else {
                amount -= 40
            }
            checkAndUpdateAmount()
        }
        balckTea = findViewById(R.id.blackTeaCheckBox)
        balckTea?.setOnClickListener {
            if(balckTea?.isChecked ?: false) {
                amount += 20
            } else {
                amount -= 20
            }
            checkAndUpdateAmount()
        }

        val cancelBtn = findViewById<Button>(R.id.cancelButton)
        cancelBtn?.setOnClickListener {
            Toast.makeText(this,"Order Canceled",Toast.LENGTH_LONG).show()
            amount = 0
            resetAll()
        }

        val orderBtn = findViewById<Button>(R.id.orderButton)
        orderBtn?.setOnClickListener {
            Toast.makeText(this,"Order Delivered",Toast.LENGTH_LONG).show()
            amount = 0
            resetAll()
        }
    }

    fun checkAndUpdateAmount() {
        coffeeAmountText?.text = if(coffee?.isChecked ?: false) "100" else "0"
        teaAmountText?.text = if(tea?.isChecked ?: false) "50" else "0"
        milkAmountText?.text = if(milk?.isChecked ?: false) "40" else "0"
        blackTeaAmountText?.text = if(balckTea?.isChecked ?: false) "20" else "0"
        amountText?.text = "$amount"
    }

    fun resetAll() {
        coffee?.isChecked = false
        tea?.isChecked = false
        milk?.isChecked = false
        balckTea?.isChecked = false
        amountText?.text = "$amount"
    }
}


